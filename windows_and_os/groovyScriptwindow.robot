#!/usr/bin/env groovy
node ('Alicia') {
    env.WORKSPACE= 'D:\\MyAis_Web\\MyAIS_Test_Web'
    env.WORKSPACE_MYAIS_WEB= 'D:\\MyAis_Web\\MyAIS_Test_Web\\myAIS_Web'
    env.WORKSPACE_MYAIS= 'D:\\MyAis_Web\\MyAIS_Test_Web\\myAIS_Web\\Script_automate'
    env.outputPath= 'D:\\MyAis_Web\\MyAIS_Test_Web\\myAIS_Web\\Results'
    env.PYBOTPATH= '/usr/local/bin/pybot'
    env.REBOTPATH= '/usr/local/bin/rebot'
    
    checkout_script()
    dir (env.WORKSPACE){
                stage('Login'){
                    run_Login_Test()
                }
    }
}

def checkout_script(){
    dir(env.WORKSPACE) {
        checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '6ce45252-ab44-445c-8d71-209f55e83fea', url: 'https://vca.ais.co.th/ATE-MYAIS/TEST/MyAis-Web.git']]])
    }
}
def getTime(){
    time = new Date().format("yyyy-MM-dd HH:mm:ss",TimeZone.getTimeZone("ICT"))
    return time
}
def run_Login_Test(){
    dir(env.WORKSPACE_MYAIS_WEB) {
        //RUN
        //bat "if exist " + env.outputPath + "/Login rm /q " + env.outputPath + "/Login/*"
        def START_TIME_EN = getTime()
        echo "Start Time EN : " + START_TIME_EN
        def ItemEN = ["3PO","3PE","3BE"]
        for (Ntype in ItemEN) {
            env.FILE_NAME= 'Results\\Login\\'+Ntype
            env.NTYPE = "$Ntype"
            try{
                bat 'pybot -d ' + env.FILE_NAME + ' -v ar_LANG:EN -v ar_NTY:'+env.NTYPE+ ' -v ar_URL:nextzy -v ar_TypeBrowser:gc '+ env.WORKSPACE_MYAIS + '\\Login.robot'
            }
            catch (exc) {
                echo 'pybot error'
            }    
        }
        def END_TIME_EN = getTime()
        echo "End Time EN : " + END_TIME_EN
        bat 'rebot --outputdir '+ outputPath +'\\Login -o output_3PO_EN.xml -l log3PO_EN.html -r report3PO_EN.html  -R -N 3PO --nostatusrc ' + outputPath +'\\Login\\3PO\\output.xml'
        bat 'rebot --outputdir '+ outputPath +'\\Login -o output_3PE_EN.xml -l log3PE_EN.html -r report3PE_EN.html  -R -N 3PE --nostatusrc ' + outputPath +'\\Login\\3PE\\output.xml'
        bat 'rebot --outputdir '+ outputPath +'\\Login -o output_3BE_EN.xml -l log3BE_EN.html -r report3BE_EN.html  -R -N 3BE --nostatusrc ' + outputPath +'\\Login\\3BE\\output.xml'

        bat 'rebot --outputdir '+ outputPath +'\\Login -o outputEN.xml -N EN -l log.html -r report.html --nostatusrc --starttime "' + START_TIME_EN + '" --endtime "' + END_TIME_EN + '" '+ outputPath +'\\Login\\output_*_EN.xml '

        def START_TIME_TH = getTime()
        echo "Start Time TH : " + START_TIME_TH
        def ItemTH = ["3PO","3PE","3BE"]
        for (Ntype in ItemTH) {
            env.FILE_NAME= 'Results\\Login\\'+Ntype
            env.NTYPE = "$Ntype"
            try{
                bat 'pybot -d ' + env.FILE_NAME + ' -v ar_LANG:TH -v ar_NTY:'+env.NTYPE+ ' -v ar_URL:nextzy -v ar_TypeBrowser:gc '+ env.WORKSPACE_MYAIS + '\\Login.robot'
            }
            catch (exc) {
                echo 'pybot error'
            }    
        }
        def END_TIME_TH = getTime()
        echo "End Time TH : " + END_TIME_TH
        bat 'rebot --outputdir '+ outputPath +'\\Login -o output_3PO_TH.xml -l log3PO_TH.html -r report3PO_TH.html  -R -N 3PO --nostatusrc ' + outputPath +'\\Login\\3PO\\output.xml'
        bat 'rebot --outputdir '+ outputPath +'\\Login -o output_3PE_TH.xml -l log3PE_TH.html -r report3PE_TH.html  -R -N 3PE --nostatusrc ' + outputPath +'\\Login\\3PE\\output.xml'
        bat 'rebot --outputdir '+ outputPath +'\\Login -o output_3BE_TH.xml -l log3BE_TH.html -r report3BE_TH.html  -R -N 3BE --nostatusrc ' + outputPath +'\\Login\\3BE\\output.xml'

        bat 'rebot --outputdir '+ outputPath +'\\Login -o outputTH.xml -N TH -l log.html -r report.html --nostatusrc --starttime "' + START_TIME_TH + '" --endtime "' + END_TIME_TH + '" '+ outputPath +'\\Login\\output_*_TH.xml '
        
        bat 'rebot --nostatusrc --outputdir '+ outputPath +'\\Login -o output.xml -N Service ' + outputPath +'\\Login\\output??.xml '
        build job: '(X001)_Login_MyAIS', propagate: false
    }
}


